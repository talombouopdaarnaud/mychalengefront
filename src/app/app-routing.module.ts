import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'homeslide',
    pathMatch: 'full'
  },
  {
    path: 'homeslide',
    loadChildren: () => import('./homeslide/homeslide.module').then( m => m.HomeslidePageModule)
  },
  {
    path: 'login',
    loadChildren: () => import('./Auth/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'tabs',
    loadChildren: () => import('./tabs/tabs.module').then( m => m.TabsPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./Auth/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'passwordrecover',
    loadChildren: () => import('./Auth/passwordrecover/passwordrecover.module').then( m => m.PasswordrecoverPageModule)
  },
  {
    path: 'numbervalidation',
    loadChildren: () => import('./Auth/numbervalidation/numbervalidation.module').then( m => m.NumbervalidationPageModule)
  },
  {
    path: 'detail',
    loadChildren: () => import('./Chalenge/detail/detail.module').then( m => m.DetailPageModule)
  },
  {
    path: 'play',
    loadChildren: () => import('./Chalenge/play/play.module').then( m => m.PlayPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./Chalenge/register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'valide/:id',
    loadChildren: () => import('./Chalenge/valide/valide.module').then( m => m.ValidePageModule)
  },
  {
    path: 'endplay/:iduser/:idchallenge',
    loadChildren: () => import('./Challenge/endplay/endplay.module').then( m => m.EndplayPageModule)
  },
  {
    path: 'paiement',
    loadChildren: () => import('./Transaction/paiement/paiement.module').then( m => m.PaiementPageModule)
  },
  {
    path: 'result/:id',
    loadChildren: () => import('./Chalenge/result/result.module').then( m => m.ResultPageModule)
  },
  {
    path: 'catchallenge/:id',
    loadChildren: () => import('./Chalenge/catchallenge/catchallenge.module').then( m => m.CatchallengePageModule)
  },
  {
    path: 'editprofil',
    loadChildren: () => import('./Auth/editprofil/editprofil.module').then( m => m.EditprofilPageModule)
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
