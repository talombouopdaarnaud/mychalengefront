import { TestBed } from '@angular/core/testing';

import { SecurityserviceService } from './securityservice.service';

describe('SecurityserviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SecurityserviceService = TestBed.get(SecurityserviceService);
    expect(service).toBeTruthy();
  });
});
