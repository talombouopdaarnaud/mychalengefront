import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EndplayPageRoutingModule } from './endplay-routing.module';

import { EndplayPage } from './endplay.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EndplayPageRoutingModule
  ],
  declarations: [EndplayPage]
})
export class EndplayPageModule {}
