import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EndplayPage } from './endplay.page';

const routes: Routes = [
  {
    path: '',
    component: EndplayPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EndplayPageRoutingModule {}
