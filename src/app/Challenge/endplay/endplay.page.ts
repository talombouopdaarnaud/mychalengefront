import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { CategorieserviceService } from '../../Service/Categorie/categorieservice.service';
import { NotificationserviceService } from 'src/app/Service/notificationservice.service';
import { ChallengeService } from '../../Service/Challenge/challenge.service';

@Component({
  selector: 'app-endplay',
  templateUrl: './endplay.page.html',
  styleUrls: ['./endplay.page.scss'],
})
export class EndplayPage implements OnInit {
  public note:string;
  public cote:string;
  public qttotal:string;
  public challenge:string;

  constructor(private active :ActivatedRoute,public Notif:NotificationserviceService,public Catservice:ChallengeService) { }

  ngOnInit() {
    let id=this.active.snapshot.paramMap.get("iduser");
    let idchallenge=this.active.snapshot.paramMap.get("idchallenge");
    this.getresult(idchallenge,id);
  }
  getresultuserofchalleng

  async getresult(idchallenge,id){
    //loading.present();
    await this.Catservice.getresultuserofchalleng(id,idchallenge)
    .subscribe((resp:any)=>{
      console.log("retour de la requete");
      this.note=resp.data[0].note;
      this.qttotal=resp.data[0].nbq;
      this.cote=resp.data[0].cote;
      this.challenge=resp.data[0].nomchalleng;
        console.log(resp);
 
    },error => {
      
      //this.Notif.presentToast(2000,"un probleme est suvenue merci de contacter l'admin","danger");
      
    });
  }

}
