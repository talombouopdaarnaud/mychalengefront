import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EndplayPage } from './endplay.page';

describe('EndplayPage', () => {
  let component: EndplayPage;
  let fixture: ComponentFixture<EndplayPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EndplayPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EndplayPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
