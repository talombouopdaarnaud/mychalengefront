import { Component, OnInit } from '@angular/core';
import { CategorieserviceService } from '../Service/Categorie/categorieservice.service';
import { NotificationserviceService } from 'src/app/Service/notificationservice.service';
import { StorageserviceService } from 'src/app/Storage/storageservice.service';
import { Storage } from '@ionic/storage';
import { ModalController,NavController } from '@ionic/angular';
import { UserserviceService } from '../Service/User/userservice.service';


@Component({
  selector: 'app-tab2',
  templateUrl: 'tab2.page.html',
  styleUrls: ['tab2.page.scss']
})
export class Tab2Page  implements OnInit{
  public loarder=true;
  public token="dddd";
  public numerook=true;
  public afficheformp1=true;
  public afficheformp2=true;

  actifform=false;
  public userinfo: any;
  segmentModel = "profil";
  showPassword = false;
  passwordToggleIcon="eye";
  constructor(public Userservice:UserserviceService,private navCtrl:NavController,public Notif:NotificationserviceService,public Catservice:CategorieserviceService,
    public storage:StorageserviceService,public storagecl:Storage,public usersev:UserserviceService ) {
      
      this.storagecl.get('token').then((token) => {
        this.token = token;
       
      });
    }
  

  ngOnInit() {
    this.userinfo =JSON.parse(localStorage.getItem("infouser"));
    

    if( this.userinfo.active){
     
      this.numerook=false;
    }else{
      this.afficheformp1=false;
      this.numerook=true;
    }
      
  }

  passwordToggle():void{
    this.showPassword=!this.showPassword;
    if(this.passwordToggleIcon=="eye"){
      this.passwordToggleIcon="eye-off-outline";
    }else{
      this.passwordToggleIcon="eye"; 
    }
  }

  async changepass(datauser){
    var userdata = {
      "password": datauser.ancienpass,
      "username": this.userinfo.username
  };

  var newuserdata = {
    "action": "resetpass",
    "newpassword": datauser.nouvaupass,
    "username": this.userinfo.username
  };
  
    console.log(userdata);

    await this.Userservice.loginuser(userdata)
    .subscribe((resp:any)=>{
    console.log(resp);
     
        if(resp.status==200){
           this.changepassword(newuserdata);
        }else{
          this.Notif.presentToast(2000,"Ancien mot de passe incorect","danger");
        }
     
        
    },error => {
      this.Notif.presentToast(2000,"Ancien mot de passe incorect","danger");
    });

  }


  async changepassword(data){
    

    this.loarder=false;

    //loading.present();
    await this.Userservice.changepassword(data)
    .subscribe((resp:any)=>{
      //console.log("retour de la requete");
      //console.log(resp.body.code);
        ///console.log(resp.code);
        
        if(resp.body.code==200){
          this.Notif.presentToast(2000,resp.body.text,"success");
          localStorage.clear();
          localStorage.setItem('slidehome',"false");
          this.navCtrl.navigateForward('/login');
          this.actifform=true;
          
        }else{
          this.Notif.presentToast(2000,resp.body.text,"danger");
        }
        //end 
        this.loarder=true;
    },error => {
      this.loarder=true;
      this.Notif.presentToast(2000,"un probleme est suvenue merci de contacter l'admin","danger");
      
    }); 
  }
  
  desableform(){
    this.afficheformp1=false;
    this.afficheformp2=true;
    this.numerook=true;

  }
  logout(){
    localStorage.clear();
    localStorage.setItem('slidehome',"false");
    this.navCtrl.navigateForward('/login');
  }

  async sendusername(){
    
    //console.log(data.username);
    this.loarder=false;
    
    //loading.present();
    await this.usersev.getcodevalidtel(this.userinfo.username)
    .subscribe((resp:any)=>{
      console.log("retour de la requete");
        console.log(resp);
        //tester si le resultat est ok 
        if(resp.code==200){
          this.Notif.presentToast(2000,resp.text,"success");
          this.afficheformp1=true;
          this.afficheformp2=false;
          
          
        }else{
          this.Notif.presentToast(2000,resp.text,"danger");
          
        }
        this.loarder=true;
    },error => {
      this.loarder=true;
      this.Notif.presentToast(2000,"un probleme est suvenue merci de contacter l'admin","danger");
      
    });
  }
  

  async valid(data){
    data.username=this.userinfo.username

    this.loarder=false;
    await this.usersev.validnumber(data)
    .subscribe((resp:any)=>{
        if(resp.body.code==200){
          this.Notif.presentToast(2000,resp.body.text,"success");
          this.afficheformp2=true;
          this.numerook=false;
          this.userinfo.active=true;
          localStorage.setItem('infouser',JSON.stringify(this.userinfo));
          
          
        }else{
          this.Notif.presentToast(2000,resp.body.text,"danger");
        }
        //end 
        this.loarder=true;
    },error => {
      this.loarder=true;
      this.Notif.presentToast(2000,"un probleme est suvenue merci de contacter l'admin","danger");
      
    });

    
  }
  
}
