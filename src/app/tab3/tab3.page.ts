import { Component,OnInit } from '@angular/core';
import { CategorieserviceService } from '../Service/Categorie/categorieservice.service';
import { NotificationserviceService } from 'src/app/Service/notificationservice.service';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit{

  public loarder=true;
  public categories: any=[];
  segmentModel = "favorites";
  constructor(public Notif:NotificationserviceService,public Catservice:CategorieserviceService ) {}
  


  ngOnInit() {
    this.getallcategorie();
  }

  async getallcategorie(){

    this.loarder=false;

    //loading.present();
    await this.Catservice.FindallCategorie()
    .subscribe((resp:any)=>{
      console.log("retour de la requete");
      this.categories=resp["hydra:member"];
        console.log(resp["hydra:member"]);
        //tester si le resultat est ok 
        // if(resp.code==200){
          
          
          
        // }else{
        //   this.Notif.presentToast(2000,resp.text,"danger");
          
        // }
        this.loarder=true;
    },error => {
      this.loarder=true;
      //this.Notif.presentToast(2000,"un probleme est suvenue merci de contacter l'admin","danger");
      
    });
  }
}
