import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
@Injectable({
  providedIn: 'root'
})
export class StorageserviceService {

  constructor(private storage: Storage) { }
  addItem(key, value){
    //this.storage.set(key,value)
    localStorage.setItem(key,JSON.stringify(value));
  }
  deleteItem(key){
    this.storage.remove(key) 
  }

  updateItem(key, newValue){
    this.storage.set(key, newValue)
    this.getAllItems()
  }

  getAllItems(){
    let tasks: any = []
    this.storage.forEach((key, value, index) => {
      alert(value);
      tasks.push({'key':value, 'value':key})
    }); 
    return tasks   
  }

  getSingleItem(name){
    let element: any 
    this.storage.get(name).then((result) => {
      
      return element;
  });
  }

  
}
