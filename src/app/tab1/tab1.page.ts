import { Component,OnInit } from '@angular/core';
import { CategorieserviceService } from '../Service/Categorie/categorieservice.service';
import { NotificationserviceService } from 'src/app/Service/notificationservice.service';
import { ModalController,NavController } from '@ionic/angular';
import { ChallengeService } from '../Service/Challenge/challenge.service';
import { NavigationExtras } from '@angular/router';
import { SecurityserviceService } from '../User/securityservice.service';
@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page implements OnInit{
  public loarder=true;
  public emptyblock=true;
  public chalenges: any=[];
  public chalengesday: any=[];
  public chalengespass: any=[];
  public chalengesfutur: any=[];
  segmentModel = "all";
  items=[
    {image:"assets/img/dragon.jpg", title:"Susan",para:"Susan is an American singer, actress, dancer and producer. In 1991, Lopez began appearing as a Fly Girl dancer on In Living Color, where she remained a regular until she decided to pursue an acting career in 1993...", albums:"100", followers:"10.50M"},
    {image:"assets/img/card-saopaolo.png", title:"Maria",para:"Susan is an American singer, actress, dancer and producer. In 1991, Lopez began appearing as a Fly Girl dancer on In Living Color, where she remained a regular until she decided to pursue an acting career in 1993...", albums:"300", followers:"2.50M"},
    {image:"assets/img/Marvel-un-retour-diron-man.jpg", title:"Emma",para:"Susan is an American singer, actress, dancer and producer. In 1991, Lopez began appearing as a Fly Girl dancer on In Living Color, where she remained a regular until she decided to pursue an acting career in 1993...", albums:"670", followers:"1.10M"},
  ];
  constructor(public security:SecurityserviceService,private navCtrl:NavController,public Notif:NotificationserviceService,public Catservice:ChallengeService) {}
  ngOnInit() {
    this.security.checksecurity();
    this.getallchallengeactif();
    this.getchallengeuser("day");
    this.getchallengeuser("pass");
    this.getchallengeuser("futur");
  }
  public segmentChanged(event){
    console.log(event.detail.value);
    if(event.detail.value=="aujourdhui"){
      this.getchallengeuser("day");
    }
    if(event.detail.value=="futur"){
      this.getchallengeuser("futur");
    }

    if(event.detail.value=="terminer"){
      this.getchallengeuser("pass");
    }

  }

  async getallchallengeactif(){
    
    this.loarder=false;

    //loading.present();
    await this.Catservice.FindChallengeactif()
    .subscribe((resp:any)=>{
      console.log("retour de la requete");
      
      this.chalenges=resp["hydra:member"];
        console.log(resp["hydra:member"]);
        
        this.loarder=true;
        //alert("pronlme de connecxion");
    },error => {
      alert("pronlme de connecxion");
      this.loarder=true;
      this.emptyblock=false;
    });
  }



  gopageplay(id){
            let navigationExtras: NavigationExtras = {
              state: {
                idchallenge: id
              }
            }
            this.navCtrl.navigateForward('/play',navigationExtras);
  }
  govalidpage(id){
    this.navCtrl.navigateForward("/valide/"+id);
  }

  async getchallengeuser(type){
    this.loarder=false;
     var user=await JSON.parse(localStorage.getItem("infouser"));
    await this.Catservice.getchallengeuser(user.id,type)
    .subscribe((resp:any)=>{
      
      if(type=="day" && resp.code==200 ){
        this.chalengesday=resp.data;
        this.loarder=true;
      }else{
        if(type=="pass" && resp.code==200 ){
          this.chalengespass=resp.data;
          this.loarder=true;
        }else{
          if(resp.code==200){
            this.chalengesfutur=resp.data;
            this.loarder=true;
          }
        }
      }
      console.log("retour de la requete");
      console.log(resp);

    },error => {
      //alert("challenge user");
     //  console.log(resp.code);
      this.loarder=true;
    });
  }

}
