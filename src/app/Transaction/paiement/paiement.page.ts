import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';

@Component({
  selector: 'app-paiement',
  templateUrl: './paiement.page.html',
  styleUrls: ['./paiement.page.scss'],
})
export class PaiementPage implements OnInit {

  link = '';
  safeResource:any;
  constructor(private nav: NavController, private router: Router, private dom: DomSanitizer) {
    this.link = this.router.getCurrentNavigation().extras.state.link || '';
    this.safeResource = this.dom.bypassSecurityTrustResourceUrl(this.link);
    console.log('Link', this.link);
   }

  ngOnInit() {
    
  }

  back() {
    this.nav.pop();
  }


}
