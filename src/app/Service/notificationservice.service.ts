import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
import { AlertController } from '@ionic/angular'; 
@Injectable({
  providedIn: 'root'
})
export class NotificationserviceService {

  constructor(public toastController: ToastController,public alertCtrl: AlertController) { }

  async presentToast(duration=2000,message,color="primary") {
    const toast = await this.toastController.create({
      message: message,
      duration: duration,
      position: 'top',
      color: color
    });
    toast.present();
  }





  async presentToastWithOptions() {
    const toast = await this.toastController.create({
      header: 'Toast header',
      message: 'Click to Close',
      position: 'top',
      buttons: [
        {
          side: 'start',
          icon: 'star',
          text: 'Favorite',
          handler: () => {
            console.log('Favorite clicked');
          }
        }, {
          text: 'Done',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    await toast.present();

    const { role } = await toast.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }



  async showAlert(header="probelme",message="",btn="OK",cssclasse="") {
    const alert = await this.alertCtrl.create({
      header: header,
      cssClass:cssclasse,
      message: message,
      buttons: [''+btn+'']
    });

    await alert.present();
  }

}
