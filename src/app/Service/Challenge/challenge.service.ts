import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ChallengeService {
  
  constructor(public httpclint:HttpClient) { }
  private apibaseendpoint="http://127.0.0.1:8000/api";
  public  token=JSON.parse(localStorage.getItem("token"));

  public FindChallengeactif(){
    return this.httpclint.get(this.apibaseendpoint+"/chalengesessions?isclose=false");
  }

  public FindChallengeactifbycat(idcat){
    return this.httpclint.get(this.apibaseendpoint+"/chalengesessions?isclose=true/&categorie.id="+idcat);
  }
 

  public Getinfochallenge(id){
    return this.httpclint.get(this.apibaseendpoint+"/chalengesessions/"+id);
  }

  public FindChallengearchive(){
    return this.httpclint.get(this.apibaseendpoint+"/chalengesessions?isclose=false");
  }

  public Registerchallenge(idchalenge,iduser){
    
    return this.httpclint.get(this.apibaseendpoint+"/parameters/register/"+iduser+"/"+idchalenge,{headers: new HttpHeaders({'Authorization':'Bearer '+this.token,  'content-type': "application/json" })});
  }

  public Initchallenge(idchallenge,iduser){
    return this.httpclint.get(this.apibaseendpoint+"/parameters/initchallenge/"+iduser+"/"+idchallenge,{headers: new HttpHeaders({'Authorization':'Bearer '+this.token,  'content-type': "application/json" })});
  }

  public Endtimerchallenge(idchallenge,iduser,time){
    return this.httpclint.get(this.apibaseendpoint+"/parameters/endtimerchallenge/"+iduser+"/"+idchallenge+"/"+time,{headers: new HttpHeaders({'Authorization':'Bearer '+this.token,  'content-type': "application/json" })});
  }

  public playquestion(dataPay){
    console.log(dataPay);
    return this.httpclint.post(this.apibaseendpoint+"/parameters/play", dataPay ,{observe:"response",headers: new HttpHeaders({'Authorization':'Bearer '+this.token,  'content-type': "application/json" })});
  }

  public getchallengeuser(iduser,type){
    return this.httpclint.get(this.apibaseendpoint+"/parameters/challengeuser/"+iduser+"/"+type,{headers: new HttpHeaders({'Authorization':'Bearer '+this.token,  'content-type': "application/json" })});
  }

  public getresultchallenge(idchallenge){
    return this.httpclint.get(this.apibaseendpoint+"/parameters/resultchallenge/"+idchallenge,{headers: new HttpHeaders({'Authorization':'Bearer '+this.token,  'content-type': "application/json" })});
  }

  public getresultuserofchalleng(iuser,idchallenge){
    return this.httpclint.get(this.apibaseendpoint+"/parameters/resultoneresult/"+idchallenge+"/"+iuser,{headers: new HttpHeaders({'Authorization':'Bearer '+this.token,  'content-type': "application/json" })});
  }
}
