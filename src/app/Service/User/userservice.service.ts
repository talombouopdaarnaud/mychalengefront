import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class UserserviceService {

  constructor(public httpclint:HttpClient) { }
  private apibaseendpoint="http://127.0.0.1:8000/api";
  public  token=JSON.parse(localStorage.getItem("token"));
  public loginuser(dataUser){
    return this.httpclint.post(this.apibaseendpoint+"/login_check", dataUser ,{observe:"response"});
  }

  public getuserbyusername(username){
    return this.httpclint.get(this.apibaseendpoint+"/users?username="+username,{headers: new HttpHeaders({'Authorization':'Bearer '+this.token,  'content-type': "application/json" })});
  }

  public registeruser(dataUser){
    return this.httpclint.post(this.apibaseendpoint+"/users", dataUser ,{observe:"response"});
  }

  public resetpassword(){

  }

  public getcoderesetpassword(username){
    return this.httpclint.get(this.apibaseendpoint+"/openapis/rememberpass/senduser/"+username);
  }

  public getcodevalidtel(username){
    return this.httpclint.get(this.apibaseendpoint+"/openapis/rememberpass/validcode/"+username);
  }

  public changepassword(dataUser){
    return this.httpclint.post(this.apibaseendpoint+"/openapis/resetpassword", dataUser ,{observe:"response"});
  }
  public validnumber(dataUser){
    return this.httpclint.post(this.apibaseendpoint+"/parameters/validcode", dataUser ,{observe:"response",headers: new HttpHeaders({'Authorization':'Bearer '+this.token,  'content-type': "application/json" })});
  }
}
