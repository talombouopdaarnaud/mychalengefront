import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class TransactionServiceService {

  constructor(public httpclint:HttpClient) { }
  private apibaseendpoint="http://127.0.0.1:8000/api";
  public  token=JSON.parse(localStorage.getItem("token"));

  public getsolde(iduser){
    return this.httpclint.get(this.apibaseendpoint+"/parameters/solde/"+iduser,{headers: new HttpHeaders({'Authorization':'Bearer '+this.token,  'content-type': "application/json" })});
  }

  

  public getalltransactionbytype(iduser,type){
    return this.httpclint.get(this.apibaseendpoint+"/transactions?user.id="+iduser+"&type="+type,{headers: new HttpHeaders({'Authorization':'Bearer '+this.token,  'content-type': "application/json" })});
  }

  public dodepot(iduser,montant){
    return this.httpclint.get(this.apibaseendpoint+"/parameters/paymentlink/"+iduser+"/"+montant,{headers: new HttpHeaders({'Authorization':'Bearer '+this.token,  'content-type': "application/json" })});
  }
  
  public doretrait(iduser,montant){
    return this.httpclint.get(this.apibaseendpoint+"/parameters/retrait/"+iduser+"/"+montant,{headers: new HttpHeaders({'Authorization':'Bearer '+this.token,  'content-type': "application/json" })});
  }CH


}
