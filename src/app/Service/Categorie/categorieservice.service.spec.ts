import { TestBed } from '@angular/core/testing';

import { CategorieserviceService } from './categorieservice.service';

describe('CategorieserviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CategorieserviceService = TestBed.get(CategorieserviceService);
    expect(service).toBeTruthy();
  });
});
