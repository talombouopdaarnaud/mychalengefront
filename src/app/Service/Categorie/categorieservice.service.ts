import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class CategorieserviceService {

  constructor(public httpclint:HttpClient) { }
  private apibaseendpoint="http://127.0.0.1:8000/api";

  public FindallCategorie(){
    return this.httpclint.get(this.apibaseendpoint+"/categories");
  }

  public Findetail(id){
    return this.httpclint.get(this.apibaseendpoint+"/categories/"+id);
  }
}
