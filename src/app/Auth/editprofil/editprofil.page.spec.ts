import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditprofilPage } from './editprofil.page';

describe('EditprofilPage', () => {
  let component: EditprofilPage;
  let fixture: ComponentFixture<EditprofilPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditprofilPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditprofilPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
