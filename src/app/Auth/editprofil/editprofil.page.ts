import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-editprofil',
  templateUrl: './editprofil.page.html',
  styleUrls: ['./editprofil.page.scss'],
})
export class EditprofilPage implements OnInit {
  public userinfo: any;
  constructor() { }

  ngOnInit() {
    
    this.userinfo =JSON.parse(localStorage.getItem("infouser"));
    console.log(this.userinfo);
  }
  edit(data){
    console.log(data);
  }

}
