import { Component, OnInit } from '@angular/core';
import { NotificationserviceService } from 'src/app/Service/notificationservice.service';
import { UserserviceService } from 'src/app/Service/User/userservice.service';
import { NavController } from '@ionic/angular';
import { StorageserviceService } from 'src/app/Storage/storageservice.service';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
 public loarder=true;
 public infouser: any=[];
  constructor(public Notif:NotificationserviceService,public Userservice:UserserviceService,
    private nav: NavController,public storage:StorageserviceService) { }

  ngOnInit() {
    
  }
  
  async login(userdata){
    this.loarder=false;

    //loading.present();
    await this.Userservice.loginuser(userdata)
    .subscribe((resp:any)=>{
      //console.log(userdata);
     // console.log("retour de la requete");
       // console.log(resp.username);
        //tester si le resultat est ok 
        if(resp.status==200){
          this.Notif.presentToast(2000,"Bienvenue dans TBAL challenge.","success");
          //console.log("response user");
          this.getandsaveuserinstorage(userdata.username);
          //console.log(this.infouser);
          this.nav.navigateForward('/tabs/tab1');
          this.storage.addItem("token",resp.body.token);
         /// console.log(resp.body.token);
        }
        //end 
        this.loarder=true;
    },error => {
      this.loarder=true;
      console.log(error);
      alert(error.message);
      alert(error.statusText);
      this.Notif.presentToast(2000,error,"danger");
      
    });
  }

  async getandsaveuserinstorage(username){
    await this.Userservice.getuserbyusername(username)
    .subscribe((resp:any)=>{
      this.storage.addItem("infouser",resp["hydra:member"][0]);
    },error => {   
    });
  }

  
}
