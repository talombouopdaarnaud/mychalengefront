import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PasswordrecoverPage } from './passwordrecover.page';

describe('PasswordrecoverPage', () => {
  let component: PasswordrecoverPage;
  let fixture: ComponentFixture<PasswordrecoverPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PasswordrecoverPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PasswordrecoverPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
