import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PasswordrecoverPage } from './passwordrecover.page';

const routes: Routes = [
  {
    path: '',
    component: PasswordrecoverPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PasswordrecoverPageRoutingModule {}
