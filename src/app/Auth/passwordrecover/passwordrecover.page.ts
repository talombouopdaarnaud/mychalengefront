import { Component, OnInit } from '@angular/core';
import { NotificationserviceService } from 'src/app/Service/notificationservice.service';
import { UserserviceService } from 'src/app/Service/User/userservice.service';
import { NavController } from '@ionic/angular';
@Component({
  selector: 'app-passwordrecover',
  templateUrl: './passwordrecover.page.html',
  styleUrls: ['./passwordrecover.page.scss'],
})
export class PasswordrecoverPage implements OnInit {
  public loarder=true;
  showPassword = false;
  passwordToggleIcon="eye";
  public username="";
  slideOpts = {  
    initialSlide: 0,  
    speed: 300,  
    effect: 'fade',  
  };    
  actifform=false;
  constructor(public Notif:NotificationserviceService,public Userservice:UserserviceService,
    private nav: NavController) { }

  ngOnInit() {
    
  }

  async sendusername(data){
    this.username=data.username;
    //console.log(data.username);
    this.loarder=false;

    //loading.present();
    await this.Userservice.getcoderesetpassword(data.username)
    .subscribe((resp:any)=>{
      console.log("retour de la requete");
        console.log(resp);
        //tester si le resultat est ok 
        if(resp.code==200){
          this.Notif.presentToast(2000,resp.text,"success");
          this.actifform=true;
          
          
        }else{
          this.Notif.presentToast(2000,resp.text,"danger");
          
        }
        this.loarder=true;
    },error => {
      this.loarder=true;
      this.Notif.presentToast(2000,"Un problème est survenu merci de contacter un admin.","danger");
      
    });
  }

  passwordToggle():void{
    this.showPassword=!this.showPassword;
    if(this.passwordToggleIcon=="eye"){
      this.passwordToggleIcon="eye-off-outline";
    }else{
      this.passwordToggleIcon="eye"; 
    }
  }

  async changepassword(data){
    data.username=this.username;

    this.loarder=false;

    //loading.present();
    await this.Userservice.changepassword(data)
    .subscribe((resp:any)=>{
      //console.log("retour de la requete");
      //console.log(resp.body.code);
        ///console.log(resp.code);
        
        if(resp.body.code==200){
          this.Notif.presentToast(2000,resp.body.text,"success");
          this.actifform=true;
          this.nav.navigateForward('/login');
        }else{
          this.Notif.presentToast(2000,resp.body.text,"danger");
        }
        //end 
        this.loarder=true;
    },error => {
      this.loarder=true;
      this.Notif.presentToast(2000,"Un problème est survenu merci de contacter un admin.","danger");
      
    });

    
  }

}
