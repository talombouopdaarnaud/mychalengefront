import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PasswordrecoverPageRoutingModule } from './passwordrecover-routing.module';

import { PasswordrecoverPage } from './passwordrecover.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PasswordrecoverPageRoutingModule
  ],
  declarations: [PasswordrecoverPage]
})
export class PasswordrecoverPageModule {}
