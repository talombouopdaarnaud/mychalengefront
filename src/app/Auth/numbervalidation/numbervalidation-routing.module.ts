import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NumbervalidationPage } from './numbervalidation.page';

const routes: Routes = [
  {
    path: '',
    component: NumbervalidationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NumbervalidationPageRoutingModule {}
