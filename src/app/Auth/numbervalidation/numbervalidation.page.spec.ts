import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NumbervalidationPage } from './numbervalidation.page';

describe('NumbervalidationPage', () => {
  let component: NumbervalidationPage;
  let fixture: ComponentFixture<NumbervalidationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NumbervalidationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NumbervalidationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
