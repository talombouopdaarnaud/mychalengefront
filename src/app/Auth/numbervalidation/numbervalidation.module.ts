import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NumbervalidationPageRoutingModule } from './numbervalidation-routing.module';

import { NumbervalidationPage } from './numbervalidation.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NumbervalidationPageRoutingModule
  ],
  declarations: [NumbervalidationPage]
})
export class NumbervalidationPageModule {}
