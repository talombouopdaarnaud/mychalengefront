import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { IonIntlTelInputModule } from 'ion-intl-tel-input';

import { RouterModule } from '@angular/router';

import { IonicModule} from '@ionic/angular';
import { RegisterPageRoutingModule } from './register-routing.module';

import { RegisterPage } from './register.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RegisterPageRoutingModule,
    //RouterModule.forChild(RegisterPage),
    IonIntlTelInputModule
  ],
  declarations: [RegisterPage]
})
export class RegisterPageModule {}
