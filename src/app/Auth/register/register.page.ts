import { Component, OnInit,ContentChild } from '@angular/core';
import { IonIntlTelInputModule } from 'ion-intl-tel-input';
import { NavController } from '@ionic/angular';
import { NotificationserviceService } from 'src/app/Service/notificationservice.service';
import { UserserviceService } from 'src/app/Service/User/userservice.service';
@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
showPassword = false;
passwordToggleIcon="eye";
public loarder=true;
  constructor(public Notif:NotificationserviceService,public Userservice:UserserviceService,
    private nav: NavController) { }

  ngOnInit() {
    
    localStorage.setItem('slidehome',"false");
  }

  passwordToggle():void{
    this.showPassword=!this.showPassword;
    if(this.passwordToggleIcon=="eye"){
      this.passwordToggleIcon="eye-off-outline";
    }else{
      this.passwordToggleIcon="eye"; 
    }
  }

  async register(userdata){
    this.loarder=false;
    console.log(userdata.telephone);
    userdata.telephone=userdata.telephone.internationalNumber;
    userdata.telephone=userdata.telephone.replace(/\s/g, "");
    console.log("valeur du trim");
    console.log(userdata.telephone);
    userdata.pays=userdata.telephone.isoCode;
    console.log(userdata);
    //loading.present();
    await this.Userservice.registeruser(userdata)
    .subscribe((resp:any)=>{
      //console.log("retour de la requete");
        console.log(resp);
        //tester si le resultat est ok 
         if(resp.status==201){
         this.Notif.presentToast(2000,"Votre inscription est terminée.","success");
         this.nav.navigateForward('/login');
          console.log(resp.body.token);
          
         }
         this.loarder=true;
        
    },error => {
      this.loarder=true;
      //alert(error);
      //alert(error.message);
      this.Notif.presentToast(2000,"Problème d'inscription(Nom d'utilisateur déjà utilisé ou numéro de téléphone déjà utilisé)","danger");
    });
  }

}
