import { Component,OnInit } from '@angular/core';
import { CategorieserviceService } from '../Service/Categorie/categorieservice.service';
import { NotificationserviceService } from 'src/app/Service/notificationservice.service';
import { ModalController,NavController } from '@ionic/angular';
import { ChallengeService } from '../Service/Challenge/challenge.service';
import { TransactionServiceService } from '../Service/User/transaction-service.service';
import { NavigationExtras } from '@angular/router';
import { LoadingController } from '@ionic/angular';
@Component({
  selector: 'app-tab4',
  templateUrl: 'tab4.page.html',
  styleUrls: ['tab4.page.scss']
})
export class Tab4Page implements OnInit{
  public loarder=false;
  public solde=0;
  public retrait: any=[];
  public depot: any=[];
  segmentModel = "solde";
  
  constructor(private navCtrl:NavController,public Notif:NotificationserviceService,
    public Catservice:ChallengeService,public Transaction:TransactionServiceService,public loadingController: LoadingController) {}
  ngOnInit() {
    this.getSolde();
    this.getretrait("retrait");
    this.getretrait("depot");

  }

  public segmentChanged(event){
    console.group(event);
  }
  

  async getSolde(){
    this.loarder=false;
    await this.Transaction.getsolde(JSON.parse(localStorage.getItem("infouser")).id)
    .subscribe((resp:any)=>{
      console.log("retour de la requete");
      
        console.log(resp);
        if(resp.code==200){
           this.solde=resp.solde;
           
        }else{
          this.Notif.presentToast(2000,"probleme de recuperation de votre solde","danger");
         
        }
        this.loarder=true;
    },error => {
     this.loarder=true;
    });
  }



  async getretrait(type){
    this.loarder=false;
    await this.Transaction.getalltransactionbytype(JSON.parse(localStorage.getItem("infouser")).id,type)
    .subscribe((resp:any)=>{
      console.log("retour de la requete");
      console.log(resp);
      
        
        if(type=="retrait"){
           this.retrait=resp["hydra:member"];
        }else{
         this.depot=resp["hydra:member"];
        }
        this.loarder=true;
    },error => {
      this.loarder=true;
    });
  }

  async dotransaction(data){
    
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Traitement .......',
      
    });
    await loading.present();

    if(data.type=="Retrait"){
      if(data.montant<100){
        this.Notif.presentToast(2000,"montant du retrait tres petit( le montant du retait doit etre de minimum 100 100 XAF)","danger");
      }else{
          await this.Transaction.doretrait(JSON.parse(localStorage.getItem("infouser")).id,data.montant)
        .subscribe((resp:any)=>{
          console.log(resp);
          if(resp.code==200){
            this.solde=resp.solde;
            this.Notif.presentToast(2000,"Votre retrait est en traitement (delais maximium 24h)","success");
          }else{
            this.Notif.presentToast(2000,resp.text,"danger");
            this.navCtrl.navigateForward('/tabs/tab4');
          }
        },error => {
          
        });
      }
      this.getretrait("retrait");
     // this.loarder=true;
    }else{

       await this.Transaction.dodepot(JSON.parse(localStorage.getItem("infouser")).id,data.montant)
      .subscribe((resp:any)=>{
        console.log(resp);
        if(resp.code==200){
          if (resp.responsse.response=="error") {
            this.Notif.presentToast(2000,"un problme est survenue","danger");
          }else{
            var link=resp.responsse.payment_url;
            let navigationExtras: NavigationExtras = {
              state: {
                link: link
              }

            }
            this.getretrait("depot");
            loading.dismiss(); 
            this.navCtrl.navigateForward('/paiement',navigationExtras);
            console.log(resp.responsse.payment_url);
          }
          
        }else{
          this.Notif.presentToast(2000,"un problme est survenue","danger");
          loading.dismiss(); 
        }

       loading.dismiss();  
      },error => {
        loading.dismiss(); 
      });
     loading.dismiss(); 
    }
   
  }

  // govalidpage(id){
  //   this.navCtrl.navigateForward("/valide/"+id);
  // }

}
