import { Component, OnInit,ViewChild} from '@angular/core';
import { IonContent, IonSlides } from '@ionic/angular';
import { ModalController,NavController } from '@ionic/angular';
@Component({
  selector: 'app-homeslide',
  templateUrl: './homeslide.page.html',
  styleUrls: ['./homeslide.page.scss'],
})
export class HomeslidePage implements OnInit {
  @ViewChild(IonSlides, {static: true}) contentSlide: IonSlides;
  slideOpts = {  
    initialSlide: 0,  
    speed: 300,  
    effect: 'fade',  
  };  
  constructor(private navCtrl:NavController) { }

  ngOnInit() {
    
    var slide=localStorage.getItem("slidehome");
    if(slide=="false"){
      this.navCtrl.navigateForward('/login');
    }
    this.contentSlide.slideTo(0);
  }
  nextslide(){
    this.contentSlide.slideNext();
  }

}
