import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeslidePage } from './homeslide.page';

const routes: Routes = [
  {
    path: '',
    component: HomeslidePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomeslidePageRoutingModule {}
