import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HomeslidePageRoutingModule } from './homeslide-routing.module';

import { HomeslidePage } from './homeslide.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    HomeslidePageRoutingModule
  ],
  declarations: [HomeslidePage]
})
export class HomeslidePageModule {}
