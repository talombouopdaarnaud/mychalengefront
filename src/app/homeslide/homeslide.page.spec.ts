import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomeslidePage } from './homeslide.page';

describe('HomeslidePage', () => {
  let component: HomeslidePage;
  let fixture: ComponentFixture<HomeslidePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomeslidePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomeslidePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
