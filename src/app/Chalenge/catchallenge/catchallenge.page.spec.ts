import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CatchallengePage } from './catchallenge.page';

describe('CatchallengePage', () => {
  let component: CatchallengePage;
  let fixture: ComponentFixture<CatchallengePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CatchallengePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CatchallengePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
