import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CatchallengePageRoutingModule } from './catchallenge-routing.module';

import { CatchallengePage } from './catchallenge.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CatchallengePageRoutingModule
  ],
  declarations: [CatchallengePage]
})
export class CatchallengePageModule {}
