import { Component,OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { CategorieserviceService } from '../../Service/Categorie/categorieservice.service';
import { NotificationserviceService } from 'src/app/Service/notificationservice.service';
import { ModalController,NavController } from '@ionic/angular';
import { ChallengeService } from '../../Service/Challenge/challenge.service';
import { NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-catchallenge',
  templateUrl: './catchallenge.page.html',
  styleUrls: ['./catchallenge.page.scss'],
})
export class CatchallengePage implements OnInit {
  public chalenges: any=[];
  constructor(private active :ActivatedRoute,private navCtrl:NavController,public Notif:NotificationserviceService,public Catservice:ChallengeService) { }

  ngOnInit() {
    let id=this.active.snapshot.paramMap.get("id");
    this.getallchallengeactif(id);
  }

  async getallchallengeactif(idcat){
    //loading.present();
    await this.Catservice.FindChallengeactifbycat(idcat)
    .subscribe((resp:any)=>{
      console.log("retour de la requete");
      this.chalenges=resp["hydra:member"];
        console.log(resp["hydra:member"]);
    },error => {
      //this.Notif.presentToast(2000,"un probleme est suvenue merci de contacter l'admin","danger");
      
    });
  }

}
