import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CatchallengePage } from './catchallenge.page';

const routes: Routes = [
  {
    path: '',
    component: CatchallengePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CatchallengePageRoutingModule {}
