import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { CategorieserviceService } from '../../Service/Categorie/categorieservice.service';
import { NotificationserviceService } from 'src/app/Service/notificationservice.service';
import { ModalController,NavController } from '@ionic/angular';
import { ChallengeService } from '../../Service/Challenge/challenge.service';
import { LoadingController } from '@ionic/angular';
import { AlertController } from '@ionic/angular'; 
import { Router } from '@angular/router';
const circleR=30;
const circleDasharray=2*Math.PI* circleR;
@Component({
  selector: 'app-play',
  templateUrl: './play.page.html',
  styleUrls: ['./play.page.scss'],
})
export class PlayPage implements OnInit {

  time: BehaviorSubject<string> = new BehaviorSubject('00:00');
  percent: BehaviorSubject<number> = new BehaviorSubject(100);
  timer: number;
  interval;
  entimervalbycorrect=false ;
  public question: any=[];
  public loarder=true;
  public score=0;
  public challengeid;
  public user:any;
  public cote=0;
  public nbq=0;
  startDuration = 1;
  state: 'start' | 'stop' = 'stop';
  circleR=circleR;
  circleDasharray=circleDasharray;

  constructor(private navCtrl:NavController, private router: Router,public alertCtrl: AlertController,public Notif:NotificationserviceService,
    public Challengeservice:ChallengeService,public loadingController: LoadingController) {

      this.challengeid = this.router.getCurrentNavigation().extras.state.idchallenge || '';
      console.log('Link', this.challengeid);
      this.user= JSON.parse(localStorage.getItem("infouser"));
    }


  startTimer(duration: number) {
    clearInterval(this.interval);
    console.log(this.interval);
    this.timer = duration * 60;
    console.log("timer pour le nouveau"+this.timer);
    this.interval=setInterval(() => {
      this.updateTimeValue();
    }, 1000);
  }
  stopTimer() {
    clearInterval(this.interval);
    this.time.next('00:00');
    this.state = 'stop';
  }

  ///timer 
  updateTimeValue() {
    let minutes: any = this.timer / 60;
    let seconds: any = this.timer % 60;
    minutes = String('0' + Math.floor(minutes)).slice(-2);
    console.log("=======================================");
    seconds = String('0' + Math.floor(seconds)).slice(-2);
    console.log(seconds);
    const text = minutes + ':' + seconds;
    this.time.next(text);
    //const totalTime =this.startDuration*60;
    //const percentage =((totalTime-this.timer)/totalTime)*100;
    //this.percent.next(percentage );
    //--this.timer;
    this.timer=this.timer-1;
    if (this.timer ==0) {
      this.stopTimer();
     // this.startTimer(1);
     if(this.entimervalbycorrect){

     }else{
       //alert(this.question.time*60);
      this.endtimerquestion(this.question.time*60);
     }
      
    }
  }


  percentageOffset(percent){
    const percentgloat=percent/100;
    return circleDasharray * (1-percentgloat);

  }

  async initquestion(){
    this.loarder=false;
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Vérification .......',
      
    });
    await loading.present();
    await this.Challengeservice.Initchallenge(this.challengeid,this.user.id)
    .subscribe((resp:any)=>{
      
        console.log(resp);
        if(resp.code==500){
          this.Notif.presentToast(2000,resp.text,"danger");
          this.stopTimer();
          this.navCtrl.navigateForward("endplay/"+this.challengeid+"/"+this.user.id);
        }
        
        if(resp.code==201){
         //alert("reprise de partie");
         if(resp.end){
          this.stopTimer();
          this.navCtrl.navigateForward("endplay/"+this.challengeid+"/"+this.user.id);
          //alert("fin du chalenge");
          }else{
            
            this.score=resp.note;
            
            
          }
        
        }

        console.log(resp.question);
        
        this.cote=resp.cote;
        this.nbq=resp.nbq;
        this.question=resp.question;
        this.startTimer(resp.question.time);
        
        this.loarder=true;
    },error => {
      this.loarder=true;
      //this.Notif.presentToast(2000,"un probleme est suvenue merci de contacter l'admin","danger");
      
    });
    await loading.dismiss();
    
  }


  async endtimerquestion(time){
    this.loarder=false;
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Question suivante .......',
      
    });
    await loading.present();
    await this.Challengeservice.Endtimerchallenge(this.challengeid,this.user.id,time)
    .subscribe((resp:any)=>{
      
        console.log(resp);
        console.log(resp.question);
        if(resp.end){
          this.stopTimer();
          this.navCtrl.navigateForward("endplay/"+this.challengeid+"/"+this.user.id);
          //alert("fin du chalenge");
        }else{
          this.question=resp.question;
          this.cote=resp.cote;
          this.startTimer(resp.question.time);
        }
        
        
    },error => {
      
      //this.Notif.presentToast(2000,"un probleme est suvenue merci de contacter l'admin","danger");
      
    });
    await loading.dismiss();
    
  }

 
  async correctionquestion(reponsse,iduser,idchallenge,idquesion){
    var cote=this.timer;
    this.entimervalbycorrect=true;
    this.stopTimer();
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Vérification .......',
      
    });
    //alert(this.user.id);
    await loading.present();
    
    await this.Challengeservice.playquestion({reponsse:reponsse,iduser:iduser,idchallenge:idchallenge,idquesion:idquesion,
    cote:cote})
    .subscribe((resp:any)=>{
      try {
          console.log(resp); 
          if(resp.body.code==200){
            if(resp.body.apreciation){
              this.bonnreponsse(resp.body.question);
            }else{
              this.mauvaisereponse(resp.body.question);
            }
            if(resp.body.end){
              this.stopTimer();
              this.navCtrl.navigateForward("endplay/"+this.user.id+"/"+this.challengeid);
             // alert("fin ducahllenge les gars");
            }else{
             // this.question=resp.body.question;
            }
            this.cote=resp.body.cote;
            this.score=resp.body.note;
          }else{
            if(resp.body.code==300){
              if(resp.body.end){
                this.stopTimer();
                this.navCtrl.navigateForward("endplay/"+this.user.id+"/"+this.challengeid);
                //alert("fin ducahllenge les gars");
              }else{
                this.question=resp.body.question;
              }
              this.cote=resp.body.cote;
              this.score=resp.body.note;
            }else{

            }
          }     
         
          
      } catch (error) {
        
      }
     loading.dismiss();  
    },error => {
      
      loading.dismiss();
      
    });
  }  



  async presentLoading() {
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Vérification .......',
      
    });
    await loading.present();

    //const { role, data } = await loading.onDidDismiss();
    return loading;
  }
  async displayloading(loading){
    await loading.dismiss();
  }
  async bonnreponsse2(){
    var message ='<div><img src="assets/img/valid.jpg" class="card-alert"></div></div><p><b>Bonne réponse!</b></p>';
    await  this.Notif.showAlert("FELECITATION",message,"CONTINUER","my-custom-class");
  }
  async mauvaisereponse2(){
    var message ='<div><img src="assets/img/bad.png" class="card-alert"></div></div><p><b>Mauvaise réponse!</b></p>';
    await this.Notif.showAlert("DESOLE",message,"CONTINUER","my-custom-class");
  }
  async mauvaisereponse(question){
    var message ='<div><img src="assets/img/bad.png" class="card-alert"></div></div><p><b>Mauvaise réponse!</b></p>';
    const alert = await this.alertCtrl.create({
      cssClass: 'my-custom-class',
      header: 'DESOLE!',
      message: message,
      buttons: [
        {
          text: 'CONTINUER',
          role: 'CONTINUER',
          cssClass: 'secondary',
          handler: (blah) => {
            this.question=question;
            this.startTimer(question.time);
            this.entimervalbycorrect=false;
          }
        }
      ]
    });

    await alert.present();
  }



  async bonnreponsse(question){
    var message ='<div><img src="assets/img/valid.jpg" class="card-alert"></div></div><p><b>Bonne réponse!</b></p>';
    const alert = await this.alertCtrl.create({
      cssClass: 'my-custom-class',
      header: 'FELECITATION!',
      message: message,
      buttons: [
        {
          text: 'CONTINUER',
          role: 'CONTINUER',
          cssClass: 'secondary',
          handler: (blah) => {
            this.question=question;
            this.startTimer(question.time);
            this.entimervalbycorrect=false;
          }
        }
      ]
    });

    await alert.present();
  }

  ngOnInit() {
    this.initquestion();
  }

}
