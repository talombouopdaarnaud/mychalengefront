import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { CategorieserviceService } from '../../Service/Categorie/categorieservice.service';
import { NotificationserviceService } from 'src/app/Service/notificationservice.service';
import { ChallengeService } from '../../Service/Challenge/challenge.service';
import { AlertController } from '@ionic/angular'; 
import { ModalController,NavController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
@Component({
  selector: 'app-valide',
  templateUrl: './valide.page.html',
  styleUrls: ['./valide.page.scss'],
})
export class ValidePage implements OnInit {
  public loarder=true;
  public chalenge: any=[];
  public imgcha: string;
  public idchallenge:string;
  constructor(private navCtrl:NavController,private active :ActivatedRoute,public Notif:NotificationserviceService,public Catservice:ChallengeService,public alertCtrl: AlertController,public loadingController: LoadingController) { }
  
  ngOnInit() {
    
    let id=this.active.snapshot.paramMap.get("id");
    this.idchallenge=id;
    this.getchallenge(id);
  }


  async getchallenge(id){
    
    this.loarder=false;

    //loading.present();
    await this.Catservice.Getinfochallenge(id)
    .subscribe((resp:any)=>{
      console.log("retour de la requete");
      this.chalenge=resp;
      this.imgcha=resp.image;
      console.log(resp.prix);
    
        
        
        this.loarder=true;
    },error => {
      this.loarder=true;
     
      
    });
  }
  async registerchallenge(){
    const loading = await this.loadingController.create({
      cssClass: 'my-custom-class',
      message: 'Traitement .......',
      
    });
    await loading.present();
    var user=JSON.parse(localStorage.getItem("infouser"));
    this.loarder=false;

    //loading.present();
    await this.Catservice.Registerchallenge(this.idchallenge,user.id)
    .subscribe((resp:any)=>{
      console.log("retour de la requete");
      console.log(resp);
      if(resp.code==200){
        this.registerok(resp.text);
        loading.dismiss(); 
        // var message ='<div><img src="assets/img/Ok-amico.svg" class="card-alert"></div></div><p>'+resp.text+'<br><b style="color:red;">Bonne chance!!!</b></p>';
        // this.Notif.showAlert("TERMINER",message,"FERMER","my-custom-class");
      }else{
        // var message ='<div><img src="assets/img/dangericon.png" class="card-alert"></div></div><p><b>'+resp.text+'</b><br></p>';
        // this.Notif.showAlert("PROBLEME",message,"FERMER","my-custom-class");
        this.registernook(resp.text);
        loading.dismiss(); 
      }
    
        
        
        this.loarder=true;
    },error => {
      this.loarder=true;
     
      
    });

  }

  async registerok(text){
    var message ='<div><img src="assets/img/Ok-amico.svg" class="card-alert"></div></div><p>'+text+'<br><b style="color:red;">Bonne chance!!!</b></p>';
    //this.Notif.showAlert("TERMINER",message,"FERMER","my-custom-class");
    const alert = await this.alertCtrl.create({
      cssClass: 'my-custom-class',
      header: 'TERMINER!',
      message: message,
      buttons: [
        {
          text: 'FERMER',
          role: 'FERMER',
          cssClass: 'secondary',
          handler: (blah) => {
            this.navCtrl.navigateForward("/tabs/tab1");
          }
        }
      ]
    });

    await alert.present();
    
  }

  async registernook(text){
    var message ='<div><img src="assets/img/dangericon.png" class="card-alert"></div></div><p><b>'+text+'</b><br></p>';
    const alert = await this.alertCtrl.create({
      cssClass: 'my-custom-class',
      header: 'PROBLEME!',
      message: message,
      buttons: [
        {
          text: 'FERMER',
          role: 'FERMER',
          cssClass: 'secondary',
          handler: (blah) => {
            this.navCtrl.navigateForward("/tabs/tab1");
          }
        }
      ]
    });

    await alert.present();
  }

}
