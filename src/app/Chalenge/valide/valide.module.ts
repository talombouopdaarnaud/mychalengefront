import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ValidePageRoutingModule } from './valide-routing.module';

import { ValidePage } from './valide.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ValidePageRoutingModule
  ],
  declarations: [ValidePage]
})
export class ValidePageModule {}
