import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ValidePage } from './valide.page';

describe('ValidePage', () => {
  let component: ValidePage;
  let fixture: ComponentFixture<ValidePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValidePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ValidePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
