import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ValidePage } from './valide.page';

const routes: Routes = [
  {
    path: '',
    component: ValidePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ValidePageRoutingModule {}
