import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import { CategorieserviceService } from '../../Service/Categorie/categorieservice.service';
import { NotificationserviceService } from 'src/app/Service/notificationservice.service';
import { ChallengeService } from '../../Service/Challenge/challenge.service';
@Component({
  selector: 'app-result',
  templateUrl: './result.page.html',
  styleUrls: ['./result.page.scss'],
})
export class ResultPage implements OnInit {
  public idchallenge:string;
  public loarder=true;
  public resut:any=[];
  constructor(private active :ActivatedRoute,public Notif:NotificationserviceService,public Catservice:ChallengeService) { }

  ngOnInit() {
    let id=this.active.snapshot.paramMap.get("id");
    this.idchallenge=id;
    this.getresultchallenge(this.idchallenge);
  }


  async getresultchallenge(idchallenge){
    //loading.present();
    this.loarder=false;
    await this.Catservice.getresultchallenge(idchallenge)
    .subscribe((resp:any)=>{
      console.log("retour de la requete");
      this.resut=resp.data;
        console.log(resp);
        this.loarder=true;
    },error => {
      this.loarder=true;
      //this.Notif.presentToast(2000,"un probleme est suvenue merci de contacter l'admin","danger");
      
    });
  }

}
